#! python2

import time  # importing time library to make Rpi wait because its too impatient
import os  # importing os library so as to communicate with the system

# os.system('stty sane')
os.system('stty -ixon')

labelOption = "Ingrese la opcion: "

# As i said it is too impatient and so if this delay is removed you will get an error
time.sleep(1)
def control():
    print("I'm Starting the motor, I hope its calibrated and armed, if not restart by giving 'x'")
    time.sleep(1)
    speed = 1500    # change your speed if you want to.... it should be between 700 - 2000
    print("Controls - a to decrease speed & d to increase speed OR q to decrease a lot of speed & e to increase a lot of speed")

    while True:
        inp = raw_input(labelOption)

        if inp == "q":
            break
        elif inp == "e":
            speed += 100    # incrementing the speed like hell
            print("speed = %d" % speed)
        elif inp == "d":
            speed += 10     # incrementing the speed
            print("speed = %d" % speed)
        elif inp == "a":
            speed -= 10     # decrementing the speed
            print("speed = %d" % speed)
        else:
            print("WHAT DID I SAID!! Press a,q,d or e")


# This is the start of the program actually, to start the function it needs to be initialized before calling... stupid python.
inp = raw_input(labelOption)
if inp == "nada":
    print('NADA')
elif inp == "control":
    control()
else:
    print("Thank You for not following the things I'm saying... now you gotta restart the program STUPID!!")
