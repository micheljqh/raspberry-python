#! python2

# This program will let you test your ESC and brushless motor.
# Make sure your battery is not connected if you are going to calibrate it at first.
# Since you are testing your motor, I hope you don't have your propeller attached to it otherwise you are in trouble my friend...?
# This program is made by AGT @instructable.com. DO NOT REPUBLISH THIS PROGRAM... actually the program itself is harmful                                             pssst Its not, its safe.

import os  # importing os library so as to communicate with the system
import time  # importing time library to make Rpi wait because its too impatient
os.system("sudo pigpiod")  # Launching GPIO library
# As i said it is too impatient and so if this delay is removed you will get an error
time.sleep(1)
import pigpio  # importing GPIO library

ESC = 4  # Connect the ESC in this GPIO pin


pi = pigpio.pi()
pi.set_servo_pulsewidth(ESC, 0)


def clear(): return os.system('clear')


# change this if your ESC's max value is different or leave it be, max = 2500
max_value = 2500
min_value = 900  # change this if your ESC's min value is different or leave it be, min = 500
variation = 5  # valor para variar la velocidad

clear()


def calibrate():
    print("\nCalibrando...")
    pi.set_servo_pulsewidth(ESC, 0)
    print("\nActivando valor maximo...")
    pi.set_servo_pulsewidth(ESC, max_value)
    time.sleep(7)
    print("\nActivando valor minimo...")
    pi.set_servo_pulsewidth(ESC, min_value)
    print("\nSentiras un tono...")
    time.sleep(7)
    print("\nEspere (2s)...")
    pi.set_servo_pulsewidth(ESC, 0)
    time.sleep(2)
    print("\nActivando ESC (3s)")
    pi.set_servo_pulsewidth(ESC, min_value)
    time.sleep(3)
    control()  # You can change this to any other function you want


def print_speed(speed):
    clear()
    print("\nEl motor comienza a moverse sobre los 960\n\n'd'(+)  'a'(-)  'min'  'max'  'stop'\n\nVelocidad = %d" % speed)


def control():
    speed = min_value
    print_speed(speed)
    while True:
        pi.set_servo_pulsewidth(ESC, speed)
        inp = raw_input(">")
        if inp == "0":
            speed = 500
            print_speed(speed)
        elif inp == "d":
            speed += variation
            print_speed(speed)
        elif inp == "a":
            speed -= variation
            print_speed(speed)
        elif inp == "min":
            speed = min_value
            print_speed(speed)
        elif inp == "max":
            speed = max_value
            print_speed(speed)
        elif inp == "stop":
            stop()  # going for the stop function
            break
        else:
            print_speed(speed)


def stop():  # This will stop every action your Pi is performing for ESC ofcourse.
    pi.set_servo_pulsewidth(ESC, 0)
    pi.stop()


calibrate()
