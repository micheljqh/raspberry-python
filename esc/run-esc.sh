#!/bin/bash

SERVER="pi@192.168.100.98"
YELLOW='\033[1;33m'
PATHTO="/home/pi/test_esc"

showMainMenu() {
	clear
	echo -e "
${YELLOW}
CONSOLA PARA SUBIR PROGRAMA DESDE WINDOWS -------------------------------------------------------------------------------

	1   - Subir y ejecutar programa
	2   - Ejecutar esc
	3   - Ejecutar test
	4   - Apagar raspbian
	q   - Salir (ctrl + c)

========================================================================================================================="

read -t120 -n3 -p $"
Escriba la opción a ejecutar (2min)? " key

case $key in
	"1")
		upload
        runProgram
        showMainMenu
		;;
	"2")
		runProgram
        showMainMenu
		;;
	"3")
		runTest
        showMainMenu
		;;
	"4")
		shutdown
        showMainMenu
		;;
	"q")
		;;
	*)
		showMainMenu
esac
}

# subir archivos
upload () {
    clear
    echo "Subiendo esc.py al servidor en la carpeta test_esc"
    scp ./esc.py "$SERVER:$PATHTO"
    scp ./test.py "$SERVER:$PATHTO"
}

# ejecutar programa
runProgram () {
    clear
    ssh -t "$SERVER" sudo killall pigpiod
    echo "Ejecutando esc"
    ssh -t "$SERVER" python "$PATHTO"/esc.py
    read -t30
}

# ejecutar test
runTest () {
    clear
    echo "Ejecutando test"
    ssh -t "$SERVER" python "$PATHTO"/test.py
    read -t30
}

# apagar raspbian
shutdown () {
    ssh -t "$SERVER" sudo shutdown -P 0
    read -t10
}

showMainMenu